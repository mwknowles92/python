import os
from sys import platform
import tarfile
import time
import logging
import pandas as pd
from pandas.io import sql as pdsql
from sqlalchemy import create_engine

def find_tar_files(tar_directory):
    """Return a list of tar files in a directory."""
    tar_files = []
    for f in os.listdir(tar_directory):
        if f.endswith('.tar'):
            tar_files.append(f)
    return tar_files

def check_tar(tar_file, engine):
    """
    See if a tar file has already been loaded or not.
    Return a list of sites that need to be loaded.
    """
    sql = 'SELECT site FROM loadftpfile WHERE tarfilename LIKE "{}"'.format(tar_file)
    sites = pd.read_sql_query(sql, engine)
    sites = sites['site'].tolist()

    # Remove sites that have already been loaded from list
    clusters = ['_x1', '_x2', '_x3', '_x4']
    if len(sites) == 0:
        logging.info('No sites have loaded {}.'.format(tar_file))
    elif len(sites) <= 4:
        for site in sites:
            logging.info('{} has already loaded {}.'.format(site, tar_file))
            clusters.remove(site)
        for cluster in clusters:
            logging.info('{} has not loaded {}.'.format(cluster, tar_file))
    else:
        raise ValueError('Number of sites incorrect: {}'.format(clusters))

    return clusters

def extract_tar(tar_file, temp_dir):
    """Extract the tar file into a temporary directory."""
    with tarfile.open(tar_file, 'r') as t:
        z_files = t.getnames()
        os.mkdir(temp_dir)
        t.extractall(temp_dir)

def uncompress_z(z):
    """
    Uncompress a .Z file from the tar file.
    Return filename without .Z extension.
    """
    if 'win' in platform:
        os.system('gzip -d ' + z)
    elif 'linux' in platform:
        os.system('uncompress ' + z)
    else:
        raise IOError("Unknown OS version.  How do I uncompress?")
    return z[:-2]

def load_to_sql(c, engine, tar_file):
    """Load the file into the SQL database."""
    cluster = c[:3] # Just the first three letters: _gX
    data = pd.read_csv(c, engine='c')
    # Work the data before loading into the db
    # First remove extra header lines
    data = data[data.cdrRecordType != 'INTEGER']
    data = data[data.cdrRecordType != 'cdrRecordType']
    # Now remove conference calls
    data = data[data.callingPartyNumber.str.startswith('b') == False]
    data = data[data.originalCalledPartyNumber.str.startswith('b') == False]
    data = data[data.finalCalledPartyNumber.str.startswith('b') == False]
    # Remove the comment column
    data.drop('comment', inplace=True, axis=1)
    # Convert the dateTime
    data.index = pd.to_datetime((data.dateTimeOrigination.values).astype(int), utc=True, unit='s').tz_convert('US/Eastern').strftime('%Y-%m-%d %H:%M:%S')

    data.to_sql(cluster, engine, if_exists='append', index_label='dateTimeOriginationConverted', chunksize=100000)

    # Mark the tar as completed for the cluster in the loadftpfile table
    sql = 'INSERT INTO loadftpfile(site,tarfilename) VALUES(%s,%s)'
    data = [cluster, tar_file]
    pdsql.execute(sql, engine, params=data)

    logging.info('{} has loaded {}'.format(cluster, tar_file))

def clean_files(temp_dir):
    """Remove created files and directories."""
    for x in os.listdir(temp_dir):
        file_path = os.path.join(temp_dir, x)
        os.remove(file_path)
    os.chdir('..')
    os.rmdir(temp_dir)

def main():
    tar_directory = os.getcwd()
    tar_files = find_tar_files(tar_directory)

    logging.info('Start.')

    with open(log_file, 'w') as f:
        for tar_file in tar_files:
            engine = create_engine('mysql+pymysql://xx:xx@xx/xx')
            clusters = check_tar(tar_file, engine)
            if len(clusters) == 0:
                logging.info('{} has already been loaded for all clusters.'.format(tar_file))
            else:
                extract_tar(tar_file, temp_dir)
                logging.info('{} extracted.'.format(tar_file))
                os.chdir(temp_dir)
                z_files = os.listdir()
                for z in z_files: # Loop through the files and load them
                    if (z[:3] in ''.join(clusters)): # Only if not already loaded
                        c = uncompress_z(z)
                        load_to_sql(c, engine, tar_file)

if __name__ == '__main__':
    # Where work is done, temp files/folders created
    temp_dir = os.path.join(os.getcwd(), '.temp')
    if os.path.exists(temp_dir):
        os.remove(temp_dir + '/*')
        os.rmdir(temp_dir)

    # Logging configuration
    day = time.localtime().tm_yday
    log_dir = os.getcwd()
    log_file = 'import.log' + str(day)
    log_path = os.path.join(log_dir, log_file)
    # If the log file already exists, increment another number
    # Add it to the file name, don't overwrite logs
    x = 1
    new_path = log_path
    while os.path.exists(log_path):
        if x > 1:
            log_path = log_path.rpartition('-')[0]
        log_path = log_path + '-' + str(x)
        x += 1
    logging.basicConfig(filename=log_path,
                        level=logging.INFO,
                        format='%(asctime)s:%(levelname)s:%(message)s',
                        datefmt='%H:%M:%S')

    try:
        main()
    except Exception as e:
        logging.error(e)
    finally:
        clean_files(temp_dir)
        logging.info('Done.')
