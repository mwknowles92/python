import logging
import pandas as pd
from pandas.io import sql as pdsql
from sqlalchemy import create_engine

def update(name, conn):
    # Set replacement value
    if name == 'NAME':
        value = 'Employee '
    elif name == 'DEPT_CUST':
        value = 'Department '
    elif name == 'DIV_BC':
        value = 'Division '
    elif name == 'DEPT_CODE':
        value = 'Department Code '
    elif name == 'DIV_CODE':
        value = 'Division Code '
    elif name == 'PBX':
        value = 'z'
    else:
        raise ValueError('Unknown name.')

    # Get data from SQL into Dataframe
    sql = 'SELECT DISTINCT {} FROM obfuscated'.format(name)
    df = pd.read_sql_query(sql, conn)

    # Generate new values and put into list
    df['new'] = '{}'.format(value) + df.index.astype(str)
    new = df['new'].tolist()
    # Escape single quotes on old values and put into list
    df[name].replace({"'":"\\'"}, regex=True, inplace=True)
    old = df[name].tolist()

    # Replace values in SQL database
    x = 0
    while x < len(old):
        sql = ('UPDATE obfuscated '
                'SET {0}=\'{1}\' WHERE {0}=\'{2}\''
                ).format(name, new[x], old[x])
        pdsql.execute(sql, conn)
        logging.debug(sql)
        x += 1

def main():
    engine = create_engine('mysql+pymysql://xx:xx@xx/xx')
    columns = ['NAME', 'DEPT_CUST', 'DIV_BC', 'DEPT_CODE', 'DIV_CODE', 'PBX']
    for column in columns:
        logging.info('Started {}'.format(column))
        update(column, engine)
        logging.info('Finished {}'.format(column))

if __name__ == '__main__':
    logging.basicConfig(filename='obfuscated.log',
                        level=logging.DEBUG,
                        format='%(asctime)s:%(levelname)s:%(message)s',
                        datefmt='%H:%M:%S')
    try:
        main()
    except Exception as e:
        logging.error(e)
