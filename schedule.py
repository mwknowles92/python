import time
import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

# Config ###################

# login
username = ''
password = ''

query = "" # visible text
site = '' # hidden value
# site = 'selectall' # hidden value

# date variables
# year, month, day
start_date = datetime.date(2016, 1, 1)
work_date = start_date
end_date = datetime.date(2016, 8, 2)
# incrementing dates
one_day = datetime.timedelta(days=1)
six_days = datetime.timedelta(days=6)
interval = one_day
# output format
format = '%d %b %Y'

# Work #####################

# start webdriver
driver = webdriver.Chrome()
driver.get("website address")

# login
form_user = driver.find_element_by_xpath('//input[@name="login"]')
form_user.clear()
form_user.send_keys(username)
form_pass = driver.find_element_by_xpath('//input[@name="password"]')
form_pass.clear()
form_pass.send_keys(password)
form_pass.send_keys(Keys.RETURN)

# close security hint
driver.find_element_by_xpath('//div[@id="logOffMsg"]//input[@name="pop2"]').click()

while work_date <= end_date:
    # select query
    select = Select(driver.find_element_by_xpath('//select[@name="queryname"]'))
    select.select_by_visible_text(query)
    driver.find_element_by_xpath('//input[@name="run"]').click()

    # select site and enter dates
    select = Select(driver.find_element_by_xpath('//select[@name="sitepbx"]'))
    select.select_by_value(site)
    form_from_date = driver.find_element_by_xpath('//input[@name="beginDate"]')
    form_from_date.clear()
    form_from_date.send_keys(work_date.strftime(format))
    if interval == six_days:
        work_date += six_days
    form_thru_date = driver.find_element_by_xpath('//input[@name="endDate"]')
    form_thru_date.clear()
    form_thru_date.send_keys(work_date.strftime(format))
    driver.find_element_by_xpath('//input[@name="execute"]').click()

    # email notification page
    driver.find_element_by_xpath('//input[@name="execute1"]').click()

    # log date to console
    print('{} completed'.format(work_date))

    work_date += one_day

    time.sleep(5)

# close the browser
driver.quit()
