import time
from sys import argv
import pandas as pd
from pandas.io import sql as pdsql
from sqlalchemy import create_engine

# TODO: Check the db for the date, and error if it's already there.

def load_to_sql(c, engine):
    """Load the file into the SQL database."""
    data = pd.read_csv(c, engine='c')
    # Add date column
    data['date'] = time.strftime('%Y-%m-%d')
    # Insert the CSV into the MySQL database
    data.to_sql('roster', engine, if_exists='append', index=False)

def main():
    roster = ''
    if len(argv) == 2:
        raise ValueError('Invalid argument. Only takes the roster file as an argument.')
        roster = argv[1]
    elif len(argv) == 1:
        roster = 'roster.csv'
    else:
        raise ValueError('Invalid argument. Only takes the roster file as an argument.')
    engine = create_engine('mysql+pymysql://xx:xx@xx/xx')
    try:
        load_to_sql(roster, engine)
    except Exception as e:
        print(e)

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
